# README

# 開発環境構築
## Git導入
- コマンドプロンプトでgitコマンドが使える状態にセットアップ

## Dockerを導入
- Windows 10 Home以前: https://qiita.com/idani/items/fb7681d79eeb48c05144
- Windows 10 Pro: https://qiita.com/ksh-fthr/items/6b1242c010fac7395a45

### ToolBoxの場合のみ
- クローン予定のフォルダを仮想マシン上で共有する必要がある。
    - https://qiita.com/gisuyama7/items/e2ed9e76cdbef798b48a
- 初期設定時に管理者権限でTerminalを起動させる必要がある
    - https://qiita.com/koteko/items/e3c77a675e21ce07dedd

## 初期化スクリプト
1. Home以前なら Docker Quickstart Terminal、ProはTerminalでクローンしたフォルダに移動
1. ./init.sh をTerminal上で実行し、ログが止まるまで待つ
1. http://localhost:3000/ でRails画面が出ること、http://localhost:7474/ でneo4jのコンソールが表示されることを確認
    - neo4jは neo4j/neo4j で初期パスワードを変更できる
    - Mysqlは root/password でログインできる
1. Ctrl+C でサーバーを停止できる

## 二回目以降
1. Home以前なら Docker Quickstart Terminal、ProはTerminalでクローンしたフォルダに移動
1. ./start-webserver.sh でサーバーを起動
1. Ctrl+C でサーバーを停止できる

# Railsコマンドの実行方法
サーバー起動中は、docker-compose up が動作している最中に、Terminalで`docker-compose exec web {実行したいコマンド}` を打つ

# DBリセット
- docker-compose down -v
- neo4j\data 以下をすべて削除する。
- init.sh から再度初期化する

# Webサーバー操作
## 再起動(既に起動済みの状態から)
- docker-compose restart web
## Railsのキャッシュクリア
- docker-compose exec web rails tmp:clear

## ターン進行
- docker-compose exec web rails runner ChronometerBatch.new.exec