echo "Compose-Up WebContainer and DBContainer."
echo "docker-compose restart"
docker-compose restart

timeout 10
echo "Install bundle"
docker-compose exec web bundle exec guard &
docker-compose exec web yarn install --no-bin-links

echo "DB Setup"
docker-compose exec web rails tmp:clear
docker-compose exec web rails db:migrate
docker-compose exec web rails db:seed_fu

echo "Precompile Asset"
docker-compose exec web rails assets:precompile &
echo "Start rails server."
echo "docker-compose run web rails server"
docker-compose exec web rails server