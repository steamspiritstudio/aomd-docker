echo "Checkout AoMD-front"
if [ ! -d "AoMD-front" ]; then
  git clone git@bitbucket.org:tennana/aomd-front.git AoMD-front
fi

echo "Compose-Up WebContainer and DBContainer."
echo "docker-compose up --build -d"
docker-compose up --build -d web

echo "Install bundle."

docker-compose exec web bundle install
docker-compose exec web yarn install --no-bin-links --check-files
docker-compose exec web rails tmp:clear

echo "Create a Database."
echo "rails db:crate"
docker-compose exec db mysql -h 127.0.0.1 -ppassword -e "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;"
docker-compose exec web rails db:create
docker-compose exec web rails db:migrate
docker-compose exec web rails db:seed_fu

echo "Precompile Asset"
docker-compose exec web rails assets:precompile

echo "Start rails server."
docker-compose up -d
echo "docker-compose exec web bundle exec guard"
docker-compose exec web bundle exec guard &
echo "docker-compose run web rails server"
docker-compose exec web rails server